# CineForum.
**INSTRUÇÕES PARA A EXECUÇÃO DO PROGRAMA:**

**1ºPasso:** Clone o repositório **https://gitlab.com/luucas-melo/ep3** em um diretório de sua preferência (ou baixe a pasta).

**2ºPasso:** Acesse o diretório do arquivo clonado e execute o comando **bundle install**, em seguida execute o comando **rake db:migrate**

**3ºPasso:** Execute o comando **rails s** para subir o servidor

**4ºPasso:** Acesse o endereço **localhost:3000** para acessar o site.

**PROPOSTA DO PROJETO:**

O cinema, a nossa sétima arte é algo que desperta o interesse de muitos. Por isso, um site será desenvolvido para que pessoas consigam dar sua opinião e discutir sobre filmes, séries e todo o mundo do cinema.

O CineForum é um site onde usuários podem dizer o que pensam e sentem sobre filmes sem medo de represária. Afinal, quem nunca quis desabafar sobre aquele filme e não conhece ninguém para fazer isso.

Os usuários cadastrados podem criar suas próprias resenhas/análises/críticas e anonimamente (com um pseudônimo) comentar sobre as resenhas postadas.

**MANUAL DO PROGRAMA:**

**O usuário Administrador:**
O usuário administrador é pré-cadastrado no sistema com usário e senha definidos, sendo eles:

 * E-mail: admin@admin
 * Senha: 123kkk

Para acessar a página de controle do administrador aperte o botão no canto superior direito para ser redirecionado.

**Usabilidade do Forum**

Qualquer pessoa poderá acessar o site e ler as discussões sobre o filme, porém para participar das discussões é necessário se registar clicando no botão superior direito da tela.

Somente o administrador tem permissão de acesso para todas as funçoes do site: editar, excluir, criar e ler.

**DADOS SOBRE AS VERSÕES DO RUBY E DO RAILS UTILIZADAS NO DESENVOLVIMENTO E GEMS UTILIZADAS:**

Versão Ruby:
2.6.3

Versão Rails:
5.1.6

## Gems:

* gem 'devise', '~> 4.4', '>= 4.4.3'
* gem 'simple_form', '~> 4.0', '>= 4.0.1'
* gem 'haml', '~> 5.0', '>= 5.0.4'
* gem 'rails_admin'
* gem 'cancancan'
* gem 'rails', '~> 5.1.6'
* gem 'sqlite3'
* gem 'puma', '~> 3.7'
